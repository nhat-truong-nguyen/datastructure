﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyQueue
{
    public class Queue<T> 
    {
        public QueueNode<T> First { get; private set; }
        public QueueNode<T> Last { get; private set; }
        public int Count { get; private set; }

        public Queue()
        {
            this.Count = 0;
        }

        public void Clear()
        {
            this.First = null;
            this.Last = null;
            this.Count = 0;
        }

        public bool Contains(T item)
        {
            bool check = false;
            for (QueueNode<T> p = this.First; p != null && !check; p = p.Next)
            {
                if (p.Value.Equals(item))
                {
                    check = true;
                }
            }
            return check;
        }

        public T Dequeue()
        {
            if (this.Count == 0)
            {
                throw new Exception("Queue is null");
            }
            QueueNode<T> node = this.First;
            this.First = this.First.Next;
            this.Count--;
            return node.Value;
        }

        public void Enqueue(T item)
        {
            QueueNode<T> newNode = new QueueNode<T>(item);
            if (this.Count == 0)
            {
                newNode.Next = this.First;
                this.First = newNode;
                this.Last = this.First;
            }
            else
            {
                this.Last.Next = newNode;
                this.Last = newNode;
            }
            this.Count++;
        }

        public T Peek()
        {
            return this.First.Value;
        }

        public void Show()
        {
            for (QueueNode<T> p = this.First; p != null; p = p.Next)
            {
                Console.Write(p.Value + " ");
            }
            Console.WriteLine();
        }
    }
}
