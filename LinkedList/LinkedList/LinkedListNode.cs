﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLinkedList
{
    class LinkedListNode<T>
    {
        public LinkedListNode(T value)
        {
            this.Next = null;
            this.Value = value;
        }

        public LinkedListNode<T> Next
        {
            get; set;
        }
        public T Value
        {
            get; set;
        }
    }
}
