﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLinkedList
{
    class Functions
    {
        /// <summary>
        /// a. Nhap danh sach lien ket don bang cach them phan tu vao cuoi danh sach
        /// </summary>
        /// <param name="list"></param>
        public static void AddMemberToList(LinkedList<int> list)
        {
            bool check = true;
            int num = 0;
            int i = 0;
            do
            {
                Console.Write($"Element {++i}: ");
                check = int.TryParse(Console.ReadLine(), out num);
                list.AddLast(num);
            } while (check);
            list.RemoveLast();
        }

        public static void DisplayList(LinkedList<int> list)
        {
            for (LinkedListNode<int> p = list.First; p != null; p = p.Next)
            {
                Console.Write(p.Value + " ");
            }
            Console.WriteLine();
        }

        private static bool IsPrimeNumber(int num)
        {
            bool check = true;
            if (num == 0 || num == 1)
            {
                check = false;
            }
            for (int i = 2; i <= num / 2 && check; i++)
            {
                if (num % i == 0)
                {
                    check = false;
                }
            }
            return check;
        }

        public static void DisplayListPrimeNumber(LinkedList<int> list)
        {
            for (LinkedListNode<int> p = list.First; p != null; p = p.Next)
            {
                if (IsPrimeNumber(p.Value))
                {
                    Console.Write(p.Value + " ");
                }
            }
            Console.WriteLine();
        }

        public static LinkedList<int> MergeTwoList(LinkedList<int> l1, LinkedList<int> l2)
        {
            LinkedList<int> newList = new LinkedList<int>();
            for (LinkedListNode<int> p = l1.First; p != null; p = p.Next)
            {
                newList.AddLast(p.Value);
            }

            for (LinkedListNode<int> p = l2.First; p != null; p = p.Next)
            {
                newList.AddLast(p.Value);
            }
            return newList;
        }

        public static LinkedList<int> ExclusiveJoinTwoList(LinkedList<int> l1, LinkedList<int> l2)
        {
            LinkedList<int> newList = new LinkedList<int>();
            for (LinkedListNode<int> p = l1.First; p != null; p = p.Next)
            {
                if (!l2.Contains(p.Value))
                {
                    newList.AddLast(p.Value);
                }
            }
            for (LinkedListNode<int> p = l2.First; p != null; p = p.Next)
            {
                if (!l1.Contains(p.Value))
                {
                    newList.AddLast(p.Value);
                }
            }
            return newList;
        }

        public static LinkedList<int> InclusiveJoinTwoList(LinkedList<int> l1, LinkedList<int> l2)
        {
            LinkedList<int> newList = new LinkedList<int>();
            for (LinkedListNode<int> p = l1.First; p != null; p = p.Next)
            {
                newList.AddLast(p.Value);
            }
            for (LinkedListNode<int> p = l2.First; p != null; p = p.Next)
            {
                if (!l1.Contains(p.Value))
                {
                    newList.AddLast(p.Value);
                }
            }
            return newList;
        }

        public static LinkedList<int> MergeSumTwoList(LinkedList<int> l1, LinkedList<int> l2)
        {
            LinkedList<int> newList = new LinkedList<int>();
            LinkedListNode<int> pL1 = l1.First;
            LinkedListNode<int> pL2 = l2.First;

            while (pL1 != null && pL2 != null)
            {
                newList.AddLast(pL1.Value + pL2.Value);
                pL1 = pL1.Next;
                pL2 = pL2.Next;
            }

            while (pL1 != null)
            {
                newList.AddLast(pL1.Value);
                pL1 = pL1.Next;
            }

            while (pL2 != null)
            {
                newList.AddLast(pL2.Value);
                pL2 = pL2.Next;
            }

            return newList;
        }
    }
}
