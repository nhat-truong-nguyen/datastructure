﻿using System;
using System.Collections;
using System.ComponentModel.Design;

namespace MyLinkedList
{
    class LinkedList<T>
    {
        public LinkedList()
        {
            First = null;
            Last = null;
            Count = 0;
        }

        public LinkedListNode<T> Last
        {
            get; private set;
        }
        public LinkedListNode<T> First
        {
            get; private set;
        }
        public int Count
        {
            get; private set;
        }

        public void AddAfter(LinkedListNode<T> node, LinkedListNode<T> newNode)
        {
            if (Count == 0)
            {
                Console.WriteLine("Empty list");
                return;
            }

            newNode.Next = node.Next;
            node.Next = newNode;

            if (node.Equals(Last))
            {
                Last = newNode;
            }

            Count++;
        }

        public LinkedListNode<T> AddAfter(LinkedListNode<T> node, T value)
        {
            LinkedListNode<T> newNode = new LinkedListNode<T>(value);
            AddAfter(node, newNode);
            return newNode;
        }

        public void AddBefore(LinkedListNode<T> node, LinkedListNode<T> newNode)
        {
            if (Count == 0)
            {
                Console.WriteLine("List empty");
                return;
            }

            if (node == First)
            {
                AddFirst(newNode);
            }
            else
            {
                AddAfter(FindPrevious(node), newNode);
            }
        }

        public LinkedListNode<T> AddBefore(LinkedListNode<T> node, T value)
        {
            LinkedListNode<T> newNode = new LinkedListNode<T>(value);
            AddBefore(node, newNode);
            return newNode;
        }

        public void AddFirst(LinkedListNode<T> node)
        {
            node.Next = First;
            First = node;

            if (this.Count == 0)
            {
                Last = node;
            }

            Count++;
        }

        public LinkedListNode<T> AddFirst(T value)
        {
            LinkedListNode<T> newNode = new LinkedListNode<T>(value);
            AddFirst(newNode);
            return newNode;
        }

        public void AddLast(LinkedListNode<T> node)
        {
            if (Count == 0)
            {
                AddFirst(node);
            }
            else
            {
                Last.Next = node;
                Last = node;
                Count++;
            }
        }

        public LinkedListNode<T> AddLast(T value)
        {
            LinkedListNode<T> newNode = new LinkedListNode<T>(value);
            AddLast(newNode);
            return newNode;
        }

        public void Clear()
        {
            First = null;
            Last = null;
            Count = 0;
        }

        public bool Contains(T value)
        {
            bool check = false;
            for (LinkedListNode<T> pointer = First; pointer != null && !check; pointer = pointer.Next)
            {
                if (pointer.Value.Equals(value))
                {
                    check = true;
                }
            }
            return check;
        }

        public LinkedListNode<T> Find(T value)
        {
            bool check = false;
            LinkedListNode<T> node = null;
            for (LinkedListNode<T> pointer = First; pointer != null && !check; pointer = pointer.Next)
            {
                if (pointer.Value.Equals(value))
                {
                    node = pointer;
                    check = true;
                }
            }
            return node;
        }

        public LinkedListNode<T> FindPrevious(LinkedListNode<T> node)
        {
            bool check = false;
            LinkedListNode<T> preNode = null;
            for (LinkedListNode<T> pointer = First; pointer.Next != null && !check; pointer = pointer.Next)
            {
                if (pointer.Next.Equals(node))
                {
                    preNode = pointer;
                    check = true;
                }
            }
            return preNode;
        }

        public LinkedListNode<T> FindLast(T value)
        {
            LinkedListNode<T> node = null;
            for (LinkedListNode<T> pointer = First; pointer.Next != null; pointer = pointer.Next)
            {
                if (pointer.Value.Equals(value))
                {
                    node = pointer;
                }
            }
            return node;
        }

        public void Remove(LinkedListNode<T> node)
        {
            if (Count == 0)
            {
                return;
            }

            if (Count == 1)
            {
                Clear();
            }
            else if (First.Equals(node))
            {
                RemoveFirst();
            }
            else if (Last.Equals(node))
            {
                RemoveLast();
            }
            else
            {
                LinkedListNode<T> preNode = FindPrevious(node);
                preNode.Next = node.Next;
                Count--;
            }     
        }

        public bool Remove(T value)
        {
            bool check = false;
            LinkedListNode<T> node = Find(value);
            if (node != null)
            {
                Remove(node);
                check = true;
            }
            return check;
        }

        public void RemoveFirst()
        {
            if (Count == 0)
            {
                return;
            }

            if (Count == 1)
            {
                Clear();
            }
            else
            {
                First = First.Next;
                Count--;
            }
        }

        public void RemoveLast()
        {
            if (Count == 0)
            {
                return;
            }

            if (Count == 1)
            {
                Clear();
            }
            else
            {
                LinkedListNode<T> node = FindPrevious(Last);
                node.Next = null;
                Last = node;
            }
        }

        internal static void view()
        {
        }
    }
}
