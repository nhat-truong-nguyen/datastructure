﻿using MyLinkedList;
using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Test
    {
        public static void Main(string[] args)
        {
            LinkedList<int> l1 = new LinkedList<int>();
            LinkedList<int> l2 = new LinkedList<int>();
            l1.AddLast(1);
            l1.AddLast(2);
            l1.AddLast(3);
            l1.AddLast(4);

            l2.AddLast(1);
            l2.AddLast(2);
            l2.AddLast(3);
            l2.AddLast(4);
            l2.AddLast(5);
            l2.AddLast(6);
            LinkedList<int> l3 = Functions.MergeSumTwoList(l1, l2);
            Functions.DisplayList(l3);
            Console.WriteLine("Count l3: " + l3.Count);
            Console.WriteLine("First l3: " + l3.First.Value);
            Console.WriteLine("Last l3: " + l3.Last.Value);
        }
    }
}
