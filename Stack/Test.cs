﻿using System;
using System.Collections;

namespace MyStack
{
    class Test
    {
        static void Main(string[] args)
        {
            Stack<int> stack = new Stack<int>();
            stack.Push(42);
            stack.Push(43);
            stack.Push(50);
            stack.Push(100);
            stack.Clear();
            stack.Show();
        }
    }
}
