﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStack
{
    public class Stack<T>
    {
        public StackNode<T> First
        {
            get;
            private set;
        }

        public int Count
        {
            get;
            private set;
        }

        public Stack()
        {
            this.First = null;
            this.Count = 0;
        }

        public bool Contains(T value)
        {
            bool check = false;
            for (StackNode<T> p = this.First; p != null && !check; p = p.Next)
            {
                if (p.Value.Equals(value))
                {
                    check = true;
                }
            }
            return check;
        }

        public void Clear()
        {
            this.First = null;
            this.Count = 0;
        }

        public void Push(T value)
        {
            StackNode<T> newNode = new StackNode<T>(value);
            newNode.Next = this.First;
            this.First = newNode;
            this.Count++;
        }

        public T Pop()
        {
            if (this.Count == 0)
            {
                throw new Exception("Stack underflow");
            }
            StackNode<T> node = this.First;
            this.First = this.First.Next;
            this.Count--;
            return node.Value;
        }

        public T Peek()
        {
            return this.First.Value;
        }

        public void Show()
        {
            for (StackNode<T> p = this.First; p != null; p = p.Next)
            {
                Console.Write(p.Value + " ");
            }
            Console.WriteLine();
        }
    }
}
