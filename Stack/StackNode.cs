﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStack
{
    public class StackNode<T>
    {
        public T Value
        {
            get; set;
        }
        public StackNode<T> Next;

        public StackNode(T value)
        {
            this.Value = value;
            this.Next = null;
        }
    }
}
